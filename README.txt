$Id$
j-Media Element

Wrapper for the j-Media Element player

Installation
------------

1. Download j-Media Element from http://www.protofunc.com/jme/index.html
2. Unzip into a directory called 'jmediaelement' in the libraries directory.
   (e.g. /sites/all/libraries/jmediaelement).
3. Install the libraries and jmediaelement modules.