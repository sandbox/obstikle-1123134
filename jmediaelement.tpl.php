<?php
// $Id$

/**
 * @file
 * Template file for the j-Media Element module
 * 
 * Variables
 *  - $element_type: the type of media element to use, i.e. video or audio.
 *  - $src: the full source path to the media file to be played.
 *  - $media_label: label for the media player.      
 */
?> 

<!-- media-player: serves as common wrapper-element -->
<div class="media-player">

  <?php if ($element_type == 'video'): ?>
    <video controls="controls" preload="none" src="<?php print $src; ?>">
      <?php // @TODO way to handle multiply sources ?>
      <p class="fallback">You need flash</p> 
		</video>
		
		<?php //<!-- media-state: has some interesting state-classes + serves as a play-pause interface for mouse --> ?>
		<div class="media-state"></div>  
  
  <?php elseif ($element_type == 'audio'): ?>
    <audio controls="controls" src="<?php print $src; ?>">
      <?php // @TODO way to handle multiply sources ?>
			<p class="fallback">You need flash</p>
		</audio>
  
  <?php endif; ?>
  
  <?php
    // jme has an interesting controls-concept, that works as a mark-up api.
    // This makes jme extremly flexible: You can change the structure, regroup etc. 
    // Please have look @http://protofunc.com/jme/documentation/documentation-controls.html
  ?>
  
  <?php //<!-- media-label for labelling a player: good for a11y --> ?>
  <span class="media-label"><?php print $media_label; ?></span>
  
  <?php //<!-- media-controls is grouping some controls --> ?>
  <div class="media-controls">
	  
    <?php //<!-- play-pause-button --> ?>
    <a class="play-pause"><span class="ui-icon"> </span><span class="button-text">play / pause</span></a>
    
    <?php //<!-- current-time: displays the current time --> ?>
    <span class="current-time player-display">00:00</span>
	  
    <?php //<!-- timeline-slider: time-slider with label (a11y.slider.ext) --> ?>
    <div class="timeline-slider">
      <span class="handle-label">play position</span>
      
      <?php //<!-- progressbar: inside of the timeline-slider --> ?>
      <div class="progressbar"></div>
    </div>
    
		<?php //<!-- duration displays duration --> ?>
    <span class="duration player-display">00:00</span>
    
    <?php //<!-- mute-unmute: toggle mute state --> ?>
    <a class="mute-unmute"><span class="ui-icon"> </span><span class="button-text">mute / unmute</span></a>
    
    <?php //<!-- volume-slider --> ?>
    <div class="volume-slider" data-mutestate="true"><span class="handle-label">volume control</span></div>
  </div>
</div><!-- //end media-player -->